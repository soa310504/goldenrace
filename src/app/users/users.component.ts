import { Component, OnInit, AfterContentInit, PipeTransform } from '@angular/core';
import { Subscription, never } from 'rxjs';
import { FormControl } from '@angular/forms'
import { DecimalPipe } from '@angular/common';
import { map, startWith } from 'rxjs/operators';

import { UsersFacadeService } from '../core/services/facades/users-facade.service';
import { UserModel } from '../core/models/user.model'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MdlComponent } from '../mdl/mdl.component'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterContentInit {
  _subs: Subscription = never().subscribe();
  users$ = this.usersFacadeService.getUsers()
  users: UserModel[] = [];
  filter = new FormControl('');
  

  constructor(
    private usersFacadeService: UsersFacadeService,
    private modalService: NgbModal
  ) {
   }

  ngOnInit(): void {
  }

  ngAfterContentInit() {
    this._subs.add(
      this.users$
      .subscribe(users => {
          console.warn("USEEEEERRRSS",users)
          this.users = users
      }
      )
    );
  }

  search(text: string, pipe: PipeTransform): UserModel[] {
    return this.users.filter(user => {
      const term = text.toLowerCase();
      return user.name.toLowerCase().includes(term)
          || pipe.transform(user.lastName).includes(term)
          || pipe.transform(user.email).includes(term)
          || pipe.transform(user.rol).includes(term);
    });
  }

  deleteUser(user: UserModel) {
    console.warn("ENTER DELETE")
    this.usersFacadeService.deleteUser(user)
  }

  open() {
    // console.warn("USER OPEN MODAL", user)
    const modalRef = this.modalService.open(MdlComponent);
    // modalRef.componentInstance.user = user;
  }

}
