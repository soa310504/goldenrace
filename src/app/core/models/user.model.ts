
export interface UserModel {
    id?: string;
    name: string;
    lastName: string;
    email: string;
    rol: string;
}