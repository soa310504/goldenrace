import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromAppRoot from '../../store/reducers/index';
import { UserModel } from '../../models/index';
import * as action from '../../store/actions/users.actions'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersFacadeService {

  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }

  createUser(user: UserModel) {
    console.warn("ENTER FACADE")
    this.store.dispatch(action.createUser({ user }))
  }

  deleteUser(user: UserModel) {
    console.warn("ENTER FACADE")
    this.store.dispatch(action.deleteUser({ user }))
  }

  getUsers(): Observable<UserModel[]> {
    return this.store.select(fromAppRoot.getUsers)
  }
}
