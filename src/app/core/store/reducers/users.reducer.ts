import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/users.actions'
import { UserModel } from '../../models/index'

export interface UserReducer {
  users: UserModel[];
}

const initialState: UserReducer = {
  users: [
    {
      id: '0',
      name: 'Sebastian',
      lastName: 'Jaramillo',
      email: 's@j.com',
      rol: 'Administrador'
    },
    {
      id: '1',
      name: 'Juan',
      lastName: 'Hurtado',
      email: 'j@h.com',
      rol: 'Auxiliar'
    },
    {
      id: '2',
      name: 'Andrea',
      lastName: 'Gomez',
      email: 'a@g.com',
      rol: 'Auditor'
    }
  ],
};

export const reducer = createReducer(
  initialState,
  // on(actions.createUser, (state, {user}) => ({ ...state, users: [...state.users, user] })),
  on(actions.createUser, (state , {user}) => {  
  console.warn("ENTER STATE!", user)
    return ({ ...state, 
      users: [...state.users, user]
    })}),
  on(actions.deleteUser, (state, {user}) => ({ ...state, users: state.users.filter(userFiltered => userFiltered.id !== user.id) })),
);

export const getUsers = (state: UserReducer) => state.users;