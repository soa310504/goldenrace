import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdlComponent } from './mdl.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DecimalPipe } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [MdlComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  exports: [
    MdlComponent,
    ReactiveFormsModule
  ],
  providers: [DecimalPipe],
  
})
export class MdlModule { }
