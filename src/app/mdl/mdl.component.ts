import { Component, OnInit, AfterContentInit, PipeTransform, Input } from '@angular/core';
import { Subscription, never } from 'rxjs';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { DecimalPipe } from '@angular/common';
import { map, startWith } from 'rxjs/operators';

import { UsersFacadeService } from '../core/services/facades/users-facade.service';
import { UserModel } from '../core/models/user.model'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MdlModule } from '../mdl/mdl.module'

@Component({
  selector: 'app-mdl',
  templateUrl: './mdl.component.html',
  styleUrls: ['./mdl.component.scss']
})
export class MdlComponent implements OnInit, AfterContentInit {
  @Input() user: UserModel = {
    id: '',
    name: '',
    lastName: '',
    email: '',
    rol: ''
  };
  formUser: FormGroup = this.fb.group({
    id: [this.user.id, [Validators.required]],
    name: [this.user.name, [Validators.required]],
    lastName: [this.user.lastName, [Validators.required]],
    email: [this.user.email, [Validators.required]],
    rol: [this.user.rol, [Validators.required]]
  });
;
  

  constructor(
    private usersFacadeService: UsersFacadeService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
   }

  ngOnInit(): void {
  }

  ngAfterContentInit() {

  }

  onSubmit(user: UserModel) {
    console.warn("USERRRRRR", user)
    this.usersFacadeService.createUser(user)
    this.modalService.dismissAll();
  }

}
